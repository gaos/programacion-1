#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 17:25:21 2020

@author: gabriel
{ key: "value"

}
{
1:"asdas"
2:"23232"

}



soy_dic = {
            'key' : "aa"
            }




print(type(soy_dic))
"""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
# traza de personas
"""
dic = {"identificador p1" : {"nombre": "nombre persona",
                             "domicilio": "su casa",
                             "ciudad": "su ciudad",
                             "contacto":["identificador", "identificador"]
                            },

       "identificador p2" : {"nombre": "nombre persona",
                             "domicilio": "su casa",
                             "ciudad": "su ciudad",
                             "contacto":["identificador", "identificador"] }
      }
"""

def save_file(dic):
  with open("personas.json", 'w') as file:
    json.dump(dic, file)


def load_file():
  with open("personas.json", 'r') as file:
    dic = json.load(file)
  return dic


def buscar(dic, texto):
    
    resultado = dic.get(texto, False)
    
    if resultado:
      print("Nombre: {}".format(resultado["nombre"]))
      print("Domicilio: {}".format(resultado["domicilio"]))
      print("Ciudad: {}".format(resultado["ciudad"]))

    return resultado


def agregar_contacto():

    rut = input("Ingrese el rut: ")
    dic = load_file()

    existe = buscar(dic, rut)

    if existe:
      contacto = existe["contacto"]
      temp = input("Ingrese el rut: ")
      existe_temp = buscar(dic, temp)
      if existe_temp:
        contacto.append(temp)
        existe["contacto"] = contacto
      else:
        print("El contacto no existe")

      dic[rut] = existe
      save_file(dic)
      #print(existe)


def mostrar_contactos():

    rut = input("Ingrese el rut: ")
    dic = load_file()

    existe = buscar(dic, rut)

    if existe:
      print("{0} tiene contacto con: ".format(dic[rut]["nombre"]))
      for i in existe["contacto"]:
        print(dic[i]["nombre"])


def ingresar(dic):
  
    dic = load_file()

    rut = input("ingrese el rut: ")
    
    existe = buscar(dic, rut)
    
    if not existe:
    
        nombre = input("Ingrese su nombre: ")
        domicilio = input("Ingrese su domicilio: ")
        ciudad = input("Ingrese su ciudad: ")
        contacto = []
        
        temp = {}
        temp["nombre"] = nombre
        temp["domicilio"] = domicilio
        temp["ciudad"] = ciudad
        temp["contacto"] = contacto
        
        dic[rut] = temp
    else:

      print("La persona {} ya existe".format(rut))

    save_file(dic)


def editar(dic):
  
  dic = load_file()
  rut = input("ingrese el rut: ")

  existe = buscar(dic, rut)

  if existe:
    nombre = input("Ingrese su nombre: ")
    domicilio = input("Ingrese su domicilio: ")
    ciudad = input("Ingrese su ciudad: ")
    contacto = []
        
    temp = {}
    temp["nombre"] = nombre
    temp["domicilio"] = domicilio
    temp["ciudad"] = ciudad
    temp["contacto"] = contacto
        
    dic[rut] = temp

    save_file(dic)


def eliminar():

  dic = load_file()
  rut = input("Ingrese un rut a eliminar")

  existe = buscar(dic, rut)

  if existe:
    del dic[rut]
    save_file(dic)

  else:
    print("El elemento no existe")

def menu():

    dic = {}
    while True:
        print("Presione:")
        print("b para buscar")
        print("i para insertar")
        print("e para editar")
        print("d para eliminar")
        print("a para agregar contacto")
        print("m para mostrar contacto")
        print("s para salir")
        opcion = input("Ingrese una opción: ")

        if opcion.upper() == "I":
            dic = ingresar(dic)

        elif opcion.upper() == "B":
          rut = input("Ingrese un RUT: ")
          dic = load_file()
          buscar(dic, rut)

        elif opcion.upper() == "E":
          dic = editar(dic)

        elif opcion.upper() == "D":
          eliminar()

        elif opcion.upper() == "A":
          agregar_contacto()
          
        elif opcion.upper() == "M":
          mostrar_contactos()

        elif opcion.upper() == "S":
            quit()

        else:
            print("La opción ingreseda no es válida, intente nuevamente")
            pass



if __name__ == "__main__":

    menu()