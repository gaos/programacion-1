#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 11:39:22 2020

que permita ingresar una key: aa y su value: estructura quimica

comparar lo ingresado con el aminoacido correcto


"""
import json

#save_file es una funcion dada por el profesor en un ejemplo
def save_file(dic):
  with open("aminoacidos.json", 'w') as file:
    json.dump(dic, file)

#funcion hecha por el profesor
def load_file():
  with open("aminoacidos.json", 'r') as file:
    dic = json.load(file)
  return dic

#funcion hecha por el profesor editada para los aminoacidos correctos
def load_file2():
  with open("aa_correctos.json", 'r') as file:
    dic = json.load(file)
  return dic

def comparador(lista, letras):
    
    i = 0
    cont = 0    
    
    """
    El i es una varible que marca el lugar en que es igual, si la cantidad
    es igual habiendo avanzado se genera la igualdad retornando el true
    el contador es para detener las igualdades
    """

    for j in range (len(lista)):

        if(i == 3 and cont == len(letras)):
            return True

        if letras[i] == lista[j] and i == 0:
            i = i + 1
            cont = cont + 1

        elif i != 0 and letras[i] != lista[j]:
            return False

        elif i != 0 and letras[i] == lista[j]:
            cont = cont + 1
            i = i +1

    if cont == len(letras):
        return True
    else:               
        return False
     

def buscador_ultimate(lista_todo_valor, letras_que_se_buscan):
    """
    Este buscador se encarga de transformar a listas
    """
    
    
    lista_de_aa = []
    indices = []
    for i in range (len(lista_todo_valor)):
        lista_de_aa.append(list(lista_todo_valor[i]))
        letras = list(letras_que_se_buscan)
    
    for j in range (len(lista_todo_valor)):
        indices.append(comparador(lista_de_aa[j],letras))
                
    
    return indices



def buscador():

    dic = load_file()
    lista_todo_valor = list(dic.values())
    print(lista_todo_valor)
    letras = input("Ingrese letras a buscar:")
    lista_de_aa = buscador_ultimate(lista_todo_valor, letras.upper())

    if len(lista_de_aa) == 0:
        print("no hay similitudes")

    else:

        i = 0
        print("Hay aminoacidos y son:")
        
        for key, value in dic.items():
#se encarga con la lista de true imprimir la key adecuada
            if lista_de_aa[i] == True:
                print(key)

            i = i + 1
            

def buscar(dic, texto):
    
    resultado = dic.get(texto, False)
    
    return resultado

def comparar(aa):
    
    dic2 = load_file2()
    
    flag = dic2.get(aa.upper(), False)
    
    return flag


def ingresar():
  
    dic = load_file()
    
    while True:
        
        aa = input("ingrese el aa: ")
        
        flag1 = comparar(aa)
        existe = buscar(dic, aa)
        
        print(flag1)
        
        dic2 = load_file2()
        estructura_buena = dic2[aa.upper()]
        
        if not existe :
        
            estructura = input("Ingrese su estructura: ")
            if estructura.upper() == estructura_buena:
                temp = estructura.upper()
                dic[aa] = temp
            else:
                print("se equivoco en la estructura, vuelva a escribir")
            if flag1 is not False:
                break
        else:
          print("Ya esta ese aminoacido {}".format(aa))
          break
      
    save_file(dic)


def editar():
  
  dic = load_file()
  aa = input("ingrese el aa: ")

  existe = buscar(dic, aa)

  if existe:
    estructura = input("Ingrese su estructura: ")        
    temp = {}
    temp["estructura"] = estructura        
    dic[aa] = temp
    save_file(dic)

def leer():
    dic = load_file()
    print(dic)

def eliminar():

  dic = load_file()
  aa = input("Ingrese el aminoacido a eliminar:")

  existe = buscar(dic, aa)

  if existe:
    del dic[aa]
    save_file(dic)

  else:
    print("El elemento no existe")

def menu():


    while True:
        print("Presione:")
        print("i para insertar")
        print("e para eliminar")
        print("d para editar estructura")
        print("l para leer el diccionario")
        print("b para buscar")
        print("s para salir")
        opcion = input("Ingrese una opción: ")

        if opcion.upper() == "I":
            ingresar()

        elif opcion.upper() == "E":
            eliminar()
            
        elif opcion.upper() == "B":
            buscador()
            
        elif opcion.upper() == "D":
            editar()
    
        elif opcion.upper() == 'L':
            leer()
            
        elif opcion.upper() == "S":
            quit()

        else:
            print("La opción ingreseda no es válida, intente nuevamente")
            pass

    print("a")


if __name__ == "__main__":

    menu()