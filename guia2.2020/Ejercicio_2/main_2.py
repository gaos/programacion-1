#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 09:30:18 2020

@author: gabriel
"""

import csv
import pandas
#obtenido de: https://realpython.com/python-csv/


df = pandas.read_csv('covid_19_data.csv', index_col='SNo')
print(df)

def load_file():
  with open('covid_19_data.csv') as csv_file:
    lista = list(csv.reader(csv_file))
  return lista

#funcion que suma segun la columna-1 querida
def suma_contagiados(lista,x):
    
    contagiados = 0
    for i in range(len(lista)):
        if i != 0 :
            contagiado = lista[i][x]
            numero = float(contagiado)
            #print(numero)
            contagiados = contagiados + numero
        else:
            pass
    return contagiados

def menu():
    lista = load_file()
    contagio = suma_contagiados(lista, 5)
    muertos = suma_contagiados(lista, 6)
    recuperados = suma_contagiados(lista,7)
    print("Contagiados:", contagio)
    print("Muertos:", muertos)
    print("Recuperados",recuperados)
    print("Estas cifras son erroneas puesto a que cada vez que fue actualizado"
          )
    print("los datos no se quitaban los anteriores del mismo lugar")

if __name__ == "__main__":
    menu()