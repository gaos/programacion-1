#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 00:36:21 2020
Ejercicio 1) cambiar key por value y value por key
@author: gabriel
"""


import json

def save_file(dic):
  with open("aa_correctos2.json", 'w') as file:
    json.dump(dic, file)
    
def load_file():
  with open("aa_correctos2.json", 'r') as file:
    dic = json.load(file)
  return dic

def menu():
    
    dic = load_file()
    dic2 = {}
    
    print(dic)
    
    #en este for se cambian los key y value
    for key, value in dic.items():
        dic2[value]= key
        
        
    print("\n\n\n",dic2)
    save_file(dic2)
    
if __name__ == "__main__":

    menu()