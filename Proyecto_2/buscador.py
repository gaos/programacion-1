#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 22:13:51 2019

@author: gabriel
"""

import random

def contador(matriz,tamano,x,y):
    contador = 0
    for i in range(tamano):
        for j in range(tamano):
            if(i+x==j+y or x+y==j+i or j==x or x==i and x != y):
                contador = contador + matriz[i][j]
    contador + matriz[y][x]
    return contador

def imprime_matriz(matriz, tamano):
    for i in range(tamano):
        for j in range(tamano):
            print(matriz[i][j], end='')
        print('')

def cambia_posicion(matriz, tamano):
    x = int(input('Ingrese coordenada x:'))
    y = int(input('Ingrese coordenada y:'))
    for i in range(tamano):
        for j in range(tamano):
            if(i+x==j+y or x+y==j+i or j==x or x==i):
                matriz[i][j] = random.randrange(1,4)

def hacer_matriz():
    matriz = []
    tamano = int(input('Ingrese el tamaño de la matriz:'))
    for i in range(tamano):
        matriz.append([0]*tamano)
    imprime_matriz(matriz, tamano)
    return matriz, tamano
    
def main():
    matriz, tamano = hacer_matriz()
    cambia_posicion(matriz, tamano)
    imprime_matriz(matriz, tamano)
    for i in range(tamano):
        for j in range(tamano):
            contado = contador(matriz, tamano, i, j)
            print('En la posición ', (i,j) ,contado)
    
if __name__=='__main__':
    main()