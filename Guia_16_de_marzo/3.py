#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 10:56:56 2020
Se pide calcular y que la lista sea [numero del estudiante, lista de notas
                                     numero...
                                     .
                                     .
                                     .
                                     ]
@author: gabriel
"""
import random

#funcion hecha para verificar
def imprime_matriz(matriz, x, y):
    
    print("Nota:")
    
    for j in range(x):
        print(j+1, end='\t')
    print("")
    
    for i in range(y):
        for j in range(x):
            print("[",matriz[i][j], end=']\t')
        print('Estudiante:', i+1)

#función que se encarga de obtener los promedios
def suma(notas):

    promedios = []
    for i in range(31):
        promedios.append(sum(notas[i])/5)
    return promedios

#realiza casi todo
def lista_de_estudiantes():

    estudiantes = []
    notas = []

    for i in range(31):
            notas.append([0]*5)
    for i in range(31):
        for j in range(5):
            notas[i][j] = round(random.uniform(1,7),1)
    #imprime_matriz(notas,5,31)
    
    for i in range(31):
        print(notas[i])
    for i in range(31):
            estudiantes.append([0]*2)
    
    for i in range(31):
        for j in range(2):
            if(j == 0):
                estudiantes[i][j] = i+1
            else:
                estudiantes[i][j] = notas[i]
    #imprime_matriz(estudiantes,2,31)
    promedios = suma(notas)
    #print(promedios)
    
    for i in range(31):
        if(promedios[i] >= 4):
            print("El estudiante:", i+1,"aprueba:", round(promedios[i],1))
        else:
            print("El estudiante:", i+1,"no aprueba:", round(promedios[i],1))

def main():
 
    try:
        lista_de_estudiantes()
        
    except:
        print("Ops, ocurrio un error")
    
if __name__=='__main__':
    main()