#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 11:58:02 2020
Realice un programa que lea una tupla que usted defina de n elementos. 
A partir de esa tupla rellene una lista (por defecto debe estar vacı́a)
 de tal forma que el primer elemento pase a ser el segundo, el segundo pase 
 a ser el tercero, el último pase a ser el primero, y ası́ sucesivamente hasta 
 recorrer toda la tupla.
Recuerde que:
tupla = ()
lista = []
@author: gabriel
"""

#función que ordena y transforma a tupla
def ordenar(tupla_1):
    lista = []
    for i in range(len(tupla_1)):
        if i == 0:
            lista.append(tupla_1[len(tupla_1)-1])
        else:
            lista.append(tupla_1[(i-1)])
        #tuple transforma lista en tupla
    return tuple(lista)


def lista_a_tupla():
    try:
        lista_1 = []
        #while para que se añadan los elementos
        while True:
            agregado = input("Añada un elemento para una futura tupla:")
            lista_1.append(agregado)
            opc = str(input("presione 1 para terminar los elementos:"))
            if opc == "1":
                break
            
        print(lista_1)
        #función que transforma listo a tupla
        tupla_1 = tuple(lista_1)
        print(tupla_1)
        print(type(tupla_1))
        
        #se llama la función que se deseaba ordenar
        tupla_2 = ordenar(tupla_1)
        
        print(tupla_2)
        print(type(tupla_2))
        
    except:
        print("Ops, ocurrio un error")


def main():
 
    lista_a_tupla()
    
if __name__=='__main__':
    main()
