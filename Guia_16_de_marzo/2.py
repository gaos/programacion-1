#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

"""
Created on Mon Mar 16 09:54:04 2020

Ejercicio 2

Se tiene un “matriz” de 15 filas y 12 columnas. Realice un programa 
en Python que permita leer la matriz, calcule y presente 
los resultados siguientes:
El menor elemento de la matriz
La suma de los elementos de las cinco primeras columnas de cada fila de la matriz.
Muestre el total de elementos negativos en las columnas de la quinta a la nueve.
Para realizar este ejercicio NO puede utilizar las funciones min, sum 
ni ninguna otra que le sean propias a las listas, se pide utilizar la lógica 
para resolver el problema.
Para rellenar la matriz puede utilizar la siguiente sintaxis de Python:
import random
random . randint ( -10 ,10)
Esto permitirá crear número aleatorios entre -10 y 10.
Por último siempre considere el concepto de Python listas sobre listas es 
similar a trabajar con matrices.
@author: gabriel
"""
#función encargada de contar las 5 primeras columnas de cada elemento
def suma_de_columnas(matriz):

    suma = []
    contador = 0
    #Puesto que son solo los 5 primeros que necesitamos no se necesita mas en j
    for i in range(12):
            for j in range(5):
                contador = contador + matriz[i][j]
            suma.append(contador)
            contador = 0
    return suma

#función que se encarga de el menor y entregar una lista de los negativos
#los negativos que son pedidos
def recorre_matriz(matriz):

    menor = 10
    menores = []
    for i in range(12):
            for j in range(15):
                if matriz[i][j] <= menor:
                    menor = matriz[i][j]
                if (matriz[i][j] < 0 and j >= 4 and j <=8):
                    menores.append(matriz[i][j])

    return menor, menores

#función encargada solo de imprimir la matriz
def imprime_matriz(matriz):
    
    print("Columnas")
    
    for j in range(15):
        print(j+1, end='\t')
    print("")
    
    for i in range(12):
        for j in range(15):
            print("[",matriz[i][j], "]", end='\t')
        print('Fila:', i+1)

#función encargada crear la matriz random
def hacer_matriz():

    matriz = []
    for i in range(12):
        matriz.append([0]*15)
    
    for i in range(12):
        for j in range(15):
            matriz[i][j] = random.randint(-10,10)
    
    return matriz


def main():
 
    try:
        matriz = hacer_matriz()
        imprime_matriz(matriz)
        menor, menores = recorre_matriz(matriz)
        

        print("El menor es:",menor)
        print("Los elementos negativos de las columnas 5 a 9 son:\n", menores)
        #puesto a que la lista tiene todos los elementos negativos solo se
        #necesita saber la cantidad de menores
        print("Y son un total de: ", len(menores))
        sumados = suma_de_columnas(matriz)
        print("Y la suma de las primeras columnas de cada fila es:\n", sumados)
        
    except:
        print("Ops, ocurrio un error")
    
if __name__=='__main__':
    main()