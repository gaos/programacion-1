#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: gabriel_onii-chan

"""

"""
2) hacer un triangulo en una matriz de [N][N*2], donde N es el tamaño del 
cateto opuesto.
"""

# Donde ocurre la magia
def crear():
    
    matriz = []
    cont = 0
    
    # Ciclo para que el tamaño sea positivo y entero
    while True:
        try:
            tamano = int(input('Ingrese el tamaño del triangulo: '))
            if(tamano > 0):
                break
            else:
                print('Valor positivo por favor')
        except:
            print('Por favor use un valor valido y entero')
    
    # Se crea la matriz
    for i in range(tamano):
        matriz.append(['X']*(tamano*2))

    for i in range(tamano):
        for j in range(tamano*2):
            
            # Se establece un rango donde será habrá una X
            if(j+1 <= cont+1 and (j+1) >= tamano*2 - cont):
                matriz[i][j] = 'X'
            elif(j > cont and (j+1) < tamano*2 - cont):
                matriz[i][j] = ' '
                
            print(matriz[i][j], end='')
            
        cont = cont + 1
        print('')

# Funcion main
def main():
    crear()

# Creo que este main es para señalar un modulo o archivo principal
if __name__ == '__main__':
    main()