#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
5) ordenar
"""
import random

def lista_cambiada(lista):
    
    lista_2 = lista[::]
    
    for i in range(len(lista_2)):
        if(lista_2[i] % 2 == 0 and lista_2[i] % 10 is not 0):
            lista_2[i] = random.randrange(30)
            
    return lista_2
    
    
def ordenar(n):
    
    # lista.sort ordena una lista de valores
    
    m = n[::]
    m.sort()
    for i in range(len(m)):
        
        # Este i+1 es para que no busque mas lejos de lo que deberia
        if(i+1 == len(m)):
            break
        if(m[i] == m[i+1]):
            del m[i]    
    o = lista_cambiada(m)
    
    return m, o

def listas():
    
    a = [1, 2, 3]
    b = [1, 5, 6]
    c = [5, 6, 7]
    n = a + b + c
    ordenadas, cambiadas = ordenar(n)
    print(a,b,c,n)
    print(ordenadas, '\n Cambiada',cambiadas)

def main():
    listas()

if __name__=='__main__':
    main()