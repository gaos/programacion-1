#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 13 17:54:07 2019

@author: gabriel
"""

def crear():
    matriz = []
    
    #Ciclo para que se realice con un tamaño adecuado
    while True:
        try:
            tamano = int(input('Ingrese el tamaño de la matriz: '))
            if(tamano > 0):
                break
            else:
                print('Valor positivo y entero, por favor')
        except:
            print('Por favor use un valor valido')
    
    # Para crear la matriz
    for i in range(tamano):
        matriz.append([0]*tamano)
    
    # Para la diagonal
    for i in range(tamano):
        for j in range(tamano):
            if(i == j):
                matriz[i][j] = 1
            elif(i is not j):
                matriz[i][j] = 0
            print(matriz[i][j], end='')
        print('')

def main():
    crear()

if __name__ == '__main__':
    main()