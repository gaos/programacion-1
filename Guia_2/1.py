#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 13 20:55:53 2019

@author: gabriel
"""

# 1
import random

def invertidor(lista):
    lista_inv = []
    
    # Genera el random de la lista en base a otra lista
    for i in range(len(lista)):
        azar = random.randrange(len(lista))
        lista_inv.append(lista[azar])
        del lista[azar]
        
    print(lista_inv)
    lista_inv.reverse()
    return lista_inv

def main():
    lista = ['onii-chan', 'yamete', 'kudasai', 'kyaa', 'anata', 'wa', 'suki']
    lista_inv = invertidor(lista)
    print(lista_inv)
    
if __name__=='__main__':
    main()