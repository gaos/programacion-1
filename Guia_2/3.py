#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 13 21:22:36 2019

@author: gabriel

3) triangulitos
"""



# Donde ocurre la magia
def crear():
    
    matriz = []
    
    
    # Ciclo para que el tamaño sea positivo y entero
    while True:
        try:
            tamano = int(input('Ingrese el tamaño del triangulo: '))
            if(tamano > 0):
                break
            else:
                print('Valor positivo por favor')
        except:
            print('Por favor use un valor valido y entero')
    
    # Se crea la matriz
    for i in range(tamano):
        matriz.append(['X']*((tamano*2)+2))

    # Triangulo invertido
    print('_'*tamano*2)
    for i in range(tamano):
        for j in range(tamano*2):
            
            # Se define la posicion
            if(j == i):
                matriz[i][j] = '\\'
            elif(j == tamano*2 - i - 1):
                matriz[i][j] = '/'
            else:
                matriz[i][j] = ' '
                
            print(matriz[i][j], end='')
            
        print('')
    
    print('\nTriangulo\n')
    # triangulo
    
    for i in range(tamano):
        for j in range(tamano*2):
            
            # Se define segun la posicion
            if(j == tamano - i-1):
                matriz[i][j] = '/'
            elif(j == tamano*2 - tamano + i):
                matriz[i][j] = '\\'
            else:
                matriz[i][j] = ' '
                
            print(matriz[i][j], end='')
            
        print('')
    print('-'*tamano*2)

    # Triangulo doble, ocupa los dos anteriores
    print("\nTriangulo doble\n")
    for i in range(tamano):
        for j in range((tamano*2)+2):
            
            # Se define segun la posicion
            if(j==0 or j == (tamano*2)+1):
                matriz[i][j] = '|'
            elif(j == i+1):
                matriz[i][j] = '\\'
            elif(j == tamano*2 - i ):
                matriz[i][j] = '/'
            else:
                matriz[i][j] = ' '
                
            print(matriz[i][j], end='')
            
        print('')
    
    
    for i in range(tamano):
        for j in range((tamano*2)+2):
            
            if(j==0 or j == (tamano*2)+1):
                matriz[i][j] = '|'
            elif(j == tamano - i):
                matriz[i][j] = '/'
            elif(j == tamano*2 - tamano + i+1):
                matriz[i][j] = '\\'
            else:
                matriz[i][j] = ' '
                
            print(matriz[i][j], end='')
            
        print('')
    

# Funcion main
def main():
    crear()

# Creo que este main es para señalar un modulo o archivo principal
if __name__ == '__main__':
    main()