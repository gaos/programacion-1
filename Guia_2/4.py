#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 13 18:20:00 2019

@author: gabriel
"""
import random


""" 
Referencia de analizador_inator:
    https://phineasyferb.fandom.com/es/wiki/Los_Inventos_de_Doofenshmirtz

"""

# Esta funcion solo es para imprimir y verlas
def imprime_listas(lista_m, lista_mayor,lista_igual, lista_raices, lista):
    
    print(lista_m, 'menores \n', lista_mayor , 'mayores \n',lista_igual,
          'iguales \n', lista_raices, 'raices\n lista:', lista)


# Esta es la funcion que se pide
def analizador_inator(lista, k, tamano):
    
    lista_menor = []
    lista_mayor = []
    lista_iguales = []
    lista_raices = []
    
    for i in range(tamano):
        
        if(lista[i] < k):
            lista_menor.append(lista[i])
        elif(lista[i] > k):
            lista_mayor.append(lista[i])
        elif(lista[i] == k):
            lista_iguales.append(lista[i])
            
        if(0 == lista[i] % k and lista[i] is not 0):
            lista_raices.append(lista[i])
            
    return lista_menor,lista_mayor,lista_iguales, lista_raices

# Funcion para probar que sirve la anterior
def listas():
    
    lista = []
    
    while True:
        
        try:
            tamano = int(input("Ingrese el tamaño de la lista:"))
            k = int(input("Ingrese un valor con el cual comparar: "))
            random_menor = int(input("Ingrese el rango menor de la lista: "))
            random_mayor = int(input("Ingrese el rango mayor de la lista: "))
            if tamano > 0:
                break
        except:
            print('Ingrese valores que deje todo bem')
            
    for i in range(tamano):
        lista.append(random.randint(random_menor, random_mayor))
        
    lista_m, lista_mayor,lista_igual, lista_raices = analizador_inator(lista, 
                                                                        k,
                                                                        tamano)
    
    imprime_listas(lista_m, lista_mayor,lista_igual, lista_raices, lista)

def main():
    listas()
    
if __name__ == '__main__':
    main()