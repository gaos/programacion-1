#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 09:42:48 2020
top spotify
@author: gabriel
"""
import csv
import json

#funcion que se encarga de leer el csv y pasarlo a lista
def load_file():
    
    with open('top50.csv') as csv_file:
        lista = list(csv.reader(csv_file))
    
    return lista

#Se encarga de guardar en json
def save_file(dic):
    
    with open("top50.json", 'w') as file:
        json.dump(dic, file)
        
#compara para obtener la posicion del mayor
def mayor(cantidad):
    
    flag = 0
    flag2 = 0
    
    for i in range(len(cantidad)):
        if i != 0:
            if cantidad[i] > flag:
                flag = cantidad[i]
                flag2 = i

    return flag2
#cuenta los repetidos
def contador_repe(lista, matriz, columna):
    
    cantidad = []
    
    for i in range(len(lista)):
        cont = 0

        for j in range(len(matriz)):
            if lista[i] == matriz[j][columna]:
                cont = cont + 1

        cantidad.append(cont)

    return cantidad
        
#compara si hay igual retorna True
def compara(lista,artista):

    for i in range(len(lista)):

        if(artista == lista[i]):
            return True

    return False
#obtiene la columna del archivo como lista
def obtener_col(matriz, columna):

    lista_col = []

    for i in range(len(matriz)):
        lista_col.append(matriz[i][columna])

    return lista_col

#se encarga de mostrar a los artista sin repetirlos        
def art():

    matriz = load_file()
    lista_art = []

    for i in range(len(matriz)):

        if i == 0:
            lista_art.append(matriz[i][2])

        else:
            flag = compara(lista_art, matriz[i][2])

            if flag == False:
                lista_art.append(matriz[i][2])

    for i in range(len(lista_art)):
        print(lista_art[i])

    print("La cantidad de artistas son:", (len(lista_art)-1))

    cantidad = contador_repe(lista_art, matriz, 2)
    mas_cantidad = mayor(cantidad)

    print("Y el artista que mas veces aparece es:", lista_art[mas_cantidad])

#obtiene la media del ruido    
def ruido():

    matriz = load_file()
    ruido = obtener_col(matriz, 7)

    for i in range(len(ruido)):
        if i !=0:
            temp = int(ruido[i])
            ruido[i] = temp
    
    #esto debio ser en una funcion, pero por tiempo no fue realizado
    temp = ruido[0]
    #se usa un valor bajo para que sea el primero en un sort
    ruido[0] = -100
    #como sort solo puede con variables numericas se cambia el valor
    ruido.sort()
    #se vuelve a dejar el valor como era en un inicio
    ruido[0] = temp

    print(ruido)

    media = int(len(ruido)/2)

    print("Y la mediana es: ",ruido[media])


#se encarga de las mas bailables y menos bps    
def item3():

    matriz = load_file()    

    col_dance = obtener_col(matriz, 6)
    bps = obtener_col(matriz, 4)
    canciones = obtener_col(matriz, 1)

    print(col_dance)
    print(bps)

    for i in range(len(col_dance)):

        if i !=0:
            temp = int(col_dance[i])
            col_dance[i] = temp
            temp = int(bps[i])
            bps[i] = temp
    
    #esto es lo que debio haber sido hecho en funcion
    temp = col_dance[0]
    col_dance[0] = -10000
    col_dance.sort()
    col_dance[0] = temp
    temp = bps[0]
    bps[0] = -10000
    bps.sort()
    bps[0] = temp
    bps.reverse()
    menos_bps = []
    mas_dance = []

#obtiene los valores mas bajos de bps y los mas altos de bailables
    for i in range(3):
        menos_bps.append(bps[i])
        mas_dance.append(col_dance[i+1])
    
    bps.reverse()
    
    print(mas_dance, menos_bps)
    
    col_dance = obtener_col(matriz, 6)
    bps = obtener_col(matriz, 4)

    for i in range(len(col_dance)):

        if i !=0:
            temp = int(col_dance[i])
            col_dance[i] = temp
            temp = int(bps[i])
            bps[i] = temp
    
    posicion_dance = []
    posicion_bps = []

    for i in range(3):

        for j in range(len(col_dance)):

            if mas_dance[i] == col_dance[j]:
                posicion_dance.append(j)

            if menos_bps[i] == bps[j]:
                posicion_bps.append(j)

#la posicion de las que tuvieron el mismo bps de las tres menores
#y la posicion de las que tenian mas valor de bailables
    print(posicion_bps,posicion_dance)

    print("Las canciones más bailable son:")

#por ello se imprimira en ocasiones las que tienen el mismo valor
    for i in range(len(posicion_dance)):
        print(canciones[posicion_dance[i]])
        
    print("Las canciones con menos tempo son:")

    for i in range(len(posicion_bps)):
        print(canciones[posicion_bps[i]])

#obtiene la mas popular y ve si es la mas ruidosa
def popular():

    matriz = load_file()
    popul = obtener_col(matriz, 13)
    ruidosa = obtener_col(matriz, 7)
    canciones = obtener_col(matriz, 1)
    
    for i in range(len(popul)):

        if i != 0:
            temp = int(popul[i])
            popul[i] = temp

#nuevamente esto debio haber sido en una funcion
    temp = popul[0]
    popul[0] = -100
    popul.sort()
    mas_popu = popul[len(popul)-1]
    popul = obtener_col(matriz, 13)

    for i in range(len(popul)):

        if i != 0:

            if mas_popu == int(popul[i]):
                flag1 = i

    print("La cancion más popular es:", canciones[flag1])    
    
    for i in range(len(ruidosa)):

        if i != 0:
            temp = int(ruidosa[i])
            ruidosa[i] = temp

#la funcion deberia transformar y ordenar a int
    temp = ruidosa[0]
    ruidosa[0] = -100
    ruidosa.sort()
    mas_ruidosa = ruidosa[len(ruidosa)-1]
    ruidosa = obtener_col(matriz, 13)
    
    for i in range(len(ruidosa)):

        if i != 0:

            if mas_ruidosa == int(ruidosa[i]):
                flag2 = i
    
    if flag1 != flag2 :
        print("Y NO es la más ruidosa")
    
    else:
        artist = obtener_col(matriz, 2)

        print("Es de:", artist[flag1])
        print("Su popularidad es:", popul[flag1])
        print("Su ruido es de:", ruidosa[flag1])
        
#crea y guarda el dicionario en json
def creacion():
    
    dic = {}
    
    save_file(dic)
    
    matriz = load_file()
    
    ranking = obtener_col(matriz, 0)
    cancion = obtener_col(matriz, 1)
    artist = obtener_col(matriz, 2)
    gen = obtener_col(matriz, 3)
    bps = obtener_col(matriz, 4)
    lenght = obtener_col(matriz, 10)
    popul= obtener_col(matriz, 13)
    
    for i in range(len(ranking)):
        dic[ranking[i]] = cancion[i],artist[i],gen[i],bps[i],lenght[i],popul[i]
    
    save_file(dic)
    
    
def menu():
    
    dic = {}
    save_file(dic)

    while True:
        print("\tBienvenido al top 50 de spotify")
        print("¿Qué desea hacer? Presione:")
        print("a. Para un listado de los artistas")
        print("b. Mediana de ruido con artistas")
        print("c. Las 3 más bailables y 3 de menor bps")
        print("d. ¿Popular es la menos ruidosa?")
        print("e. Generar json y mostrar a los artistas")
        print("s. Para salir")
        
        opc = input("Su opción es:")
        
        if opc.upper() == "A":
            art()
    
        elif opc.upper() == "B":
            ruido()
                
        elif opc.upper() == "C":
            item3()
                
        elif opc.upper() == "D":
            popular()
        
        elif opc.upper() == 'E':
            creacion()
       
        elif opc.upper() == "S":
            quit()
    
        else:
            print("La opción ingreseda no es válida, intente nuevamente")
            pass
        
    
if __name__ == "__main__":
    menu()